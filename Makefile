#	$OpenBSD: Makefile,v 1.16 2017/07/09 14:04:50 espie Exp $

# -DEXTENDED 
# 	if you want the paste & spaste macros.

.include <bsd.own.mk>

PROG=	m4
CFLAGS+=-DEXTENDED -I. -I${.CURDIR}/lib
CDIAGFLAGS=-W -Wall -Wstrict-prototypes -pedantic \
	-Wno-unused -Wno-char-subscripts -Wno-sign-compare

YHEADER=1
LDADD= -lm -lutil
DPADD= ${LIBM} ${LIBUTIL}

SRCS=	lib/ohash.c compat.c eval.c expr.c look.c main.c misc.c gnum4.c trace.c tokenizer.l parser.y
MAN=	m4.1

GENFILES=	tokenizer.c parser.c parser.h

CLEANFILES+=	parser.c parser.h tokenizer.o tokenizer.c

tokenizer.o: parser.h

.include <bsd.prog.mk>